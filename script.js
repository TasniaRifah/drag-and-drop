const items = document.querySelectorAll('.item')
const columns = document.querySelectorAll('.column')
let dragItem = null;
items.forEach(item => {
    item.addEventListener('dragstart', dragStart);
    item.addEventListener('dragend', dragEnd);
});
columns.forEach(column => {

    column.addEventListener('dragover', dragOver);
    // column.addEventListener('dragenter', dragEnter);
    // column.addEventListener('dragleave', dragLeave);
    column.addEventListener('drop', dragDrop);
    new Sortable(column, {
        group: "shared",
        animation: 150,
        ghostClass: "blue-background-class"
    });
});
function dragOver(e) {
    e.preventDefault()

}


function dragStart() {
    dragItem = this;
    if (this.id == 'title') {
        let tag = document.createElement('h1');
        tag.innerHTML = 'This is heading.';
        this.textContent = " ";
        this.appendChild(tag);

    }
    if (this.id == 'paragraph') {
        let tag = document.createElement('p');
        tag.innerHTML = 'This is paragraph';
        this.textContent = " ";
        this.appendChild(tag);

    }
    if (this.id == 'button') {
        let tag = document.createElement('button');
        tag.innerHTML = 'Button';
        this.textContent = " ";
        this.appendChild(tag);

    }
    if (this.id == 'link') {
        let tag = document.createElement('a');
        tag.setAttribute('href', 'https://youtube.com/')
        tag.innerHTML = 'Link';

        this.textContent = " ";
        this.appendChild(tag);

    }
   if (this.id == 'img') {
        let tag = document.createElement('img');
        tag.setAttribute('src', '../img.jpg');
        tag.setAttribute('height', '200');
        tag.setAttribute('width', '200');


        this.textContent = " ";
        this.appendChild(tag);

    }

    if (this.id == 'ol') {
        let tag = document.createElement('ol');
        for (let i = 1; i <= 3; i++) {

            let list = document.createElement('li');
            list.innerHTML = 'List ' + i;
            tag.appendChild(list);
        }


        this.textContent = " ";
        this.appendChild(tag);
        
    }
    if (this.id == 'ul') {
        let tag = document.createElement('ul');
        for (let i = 1; i <= 3; i++) {

            let list = document.createElement('li');
            list.innerHTML = 'List ' + i;
            tag.appendChild(list);
        }


        this.textContent = " ";
        this.appendChild(tag);
        
    }
    if (this.id == 'form') {
        let tag = document.createElement('form');
        let name = document.createElement('label');
        name.innerHTML = "Name: ";
        let nameInput = document.createElement('input');
        nameInput.setAttribute('type', 'text');
        let pass = document.createElement('label');
        pass.innerHTML = "<br> <br> password: ";
        let passInput = document.createElement('input');
        passInput.setAttribute('type', 'password');
        let button = document.createElement("button");
        button.innerHTML = 'Submit';
        button.setAttribute('type', 'submit');
        let br = document.createElement('br');
        tag.appendChild(name);
        tag.appendChild(nameInput);

        tag.appendChild(pass)
        tag.appendChild(passInput)
        tag.appendChild(br)

        tag.appendChild(button)
        this.textContent = " ";
        this.appendChild(tag);
        // setTimeout(() => this.className = 'invisible', 0)
    }
    if (this.id == 'table') {
        let tag = document.createElement("table")
        tag.setAttribute("border","solid")
        let thead = document.createElement('thead');
        thead.setAttribute("border", 'solid')
        for (let i = 1; i <= 3; i++) {

            let col = document.createElement('th');
            col.innerHTML = 'heading' + i;
            thead.appendChild(col);
        }
        tag.appendChild(thead);
        let tbody = document.createElement('tbody');
        for (let j = 1; j <= 4; j++) {
            let row = document.createElement('tr');
            for (let i = 1; i <= 3; i++) {

                let col = document.createElement('td');
                col.innerHTML = 'col' + i + j;
                row.appendChild(col);
            }
            tbody.appendChild(row);
        }
        tag.appendChild(tbody);

        this.textContent = " ";
        this.appendChild(tag);
     
    }
    setTimeout(() => this.className = 'invisible', 0)
}

function dragEnd() {

    this.className = 'item'
    dragItem = null;
}

function dragDrop() { 
    this.append(dragItem);

}

